const Authentication = require('./controllers/authentication');
const Product = require('./controllers/product');
const Card = require('./controllers/card');

const passportService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function (app) {

  app.post('/signIn', requireSignin, Authentication.signIn);
  app.post('/signUp', Authentication.signUp);
  app.get('/getKey', Authentication.getKey);

  app.get('/getProductsList', Product.getProductsList);
  app.get('/getProductDetailsByID', Product.getProductDetailsByID);

  app.get('/getUsersPaymentInfo', Card.getUsersPaymentInfo);
  app.post('/updateUserPaymentInfo', Card.updateUserPaymentInfo);

};
