const { getDBConn } = require('../config');
const Promise = require("bluebird");

exports.getUsersPaymentInfo = function (req, res, next) {

 const username = req.query.username;
 selectSQL = `SELECT stripe_cust_id, stripe_card_id, stripe_token_id, last4 from users where username = '${username}' LIMIT 1`;

 Promise.using(getDBConn(), function(connection) {
      return connection.query(selectSQL)
            .then(function(row) {
              console.log(row);
              res.json({ item: row });

            }).catch(function(error) {
              console.log(error);
              return next(error);
            });
    })
};  


exports.updateUserPaymentInfo = function (req, res, next) {

	const username 	= req.body.username;
	const customer 	= req.body.customer;
	const token     = req.body.token;
	const card 		= req.body.card;
	const last4   	= req.body.last4;

	updateSQL = `UPDATE users SET 
					stripe_cust_id = '${customer}', 
					stripe_card_id = '${card}', 
					stripe_token_id = '${token}',
					last4 = '${last4}' 
				 WHERE 
				 	username = '${username}'`;

 	Promise.using(getDBConn(), function(connection) {
      return connection.query(updateSQL)
            .then(function(row) {
            	
              res.json({ affectedRows: row.affectedRows });

            }).catch(function(error) {
              console.log(error);
              return next(error);
            });
    })

};  



