const { getDBConn } = require('../config');
const Promise = require("bluebird");

exports.getProductsList = function (req, res, next) {

 selectSQL = 'SELECT * from products';

 Promise.using(getDBConn(), function(connection) {
      return connection.query(selectSQL)
            .then(function(rows) {
              console.log(rows);
              res.json({ list: rows });

            }).catch(function(error) {
              console.log(error);
              return next(error);
            });
    })
};  


exports.getProductDetailsByID = function (req, res, next) {

 const id = req.query.id;
 selectSQL = 'SELECT * from products where id = ${id}';

 Promise.using(getDBConn(), function(connection) {
      return connection.query(selectSQL)
            .then(function(row) {
              console.log(row);
              res.json({ item: row });

            }).catch(function(error) {
              console.log(error);
              return next(error);
            });
    })
};  

