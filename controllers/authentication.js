const jwt = require('jwt-simple');
const { getDBConn, key } = require('../config');
const bcrypt = require('bcrypt-nodejs');
var Promise = require("bluebird");

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user, iat: timestamp }, key.secret);
}

exports.signIn = function (req, res, next) {
  // User has already had their email and password auth'd
  // We just need to give them a token
  // See if a user with the given email exists
    Promise.using(getDBConn(), function(connection) {
        const email = req.body.email;
        return connection.query(`SELECT username, title FROM users INNER JOIN roles 
                ON ( users.role_id = roles.id) WHERE username = "${email}"`)
        .then(function(rows) {
         
            if (rows.length > 0) {
                return res.send({ token: tokenForUser(req.email), type: rows[0].title.toLowerCase() });
            }

            res.json({ token: tokenForUser(req.email) });

        }).catch(function(error) {
          console.log(error);
          return false;
        });
    })
}

exports.signUp = function (req, res, next) {
  const email     = req.body.email;
  const password  = req.body.password;
  const name      = req.body.name;


  if (!email || !password) {
    return res.status(422).send({ error: 'You must provide email and password' });
  }

  // See if a user with the given email exists
  Promise.using(getDBConn(), function(connection) {
      return connection.query(`SELECT * from users WHERE username = '${email}' limit 1`)
            .then(function(rows) {
              if (rows.length > 0) {
                  return res.status(422).send({ error: 'Email is in use' });
              }

              // generate a salt then run callback
              bcrypt.genSalt(10, function(err, salt) {
                if (err) { return false; }

                // hash (encrypt) our password using the salt
                bcrypt.hash(password, salt, null, function(err, hash) {
                  if (err) { return false; }
             
                // If a user with email does NOT exist, create and save user record

                return connection.query(`INSERT INTO users 
                    (firstname, username, password_hash, role_id, status) VALUES ( 
                      '${name}','${email}','${hash}', 2, 1 )`)
                .then(function(rows) {

                     // Repond to request indicating the user was created
                    res.json({ message: "The user information was updated successfully."});
                  });
               });
            });

            }).catch(function(error) {
              console.log(error);
              return false;
            });
    })
};

exports.getKey = function (req, res, next) {

  res.json({ key: 'sk_test_7BahJCDxN6s8dxyEh3ieTc5d' });

};  
