// Load module
var mysql = require('promise-mysql');
// Initialize pool
var pool      =    mysql.createPool({
    connectionLimit : 10,
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'smoothies',
    debug    :  false
});    
 
function getDBConnection() {
  return pool.getConnection().disposer(function(connection) {
    pool.releaseConnection(connection);
  });
}

var key = { secret: 'wejgfehvasdbvjkadblksnlq' };

exports.getDBConn = getDBConnection;
exports.key = key;
