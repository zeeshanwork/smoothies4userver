const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

const { getDBConn, key } = require('../config');
const bcrypt = require('bcrypt-nodejs');
var Promise = require("bluebird");

// Create local strategy
const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, function(email, password, done) {
  // Verify this email and password, call done with the user
  // if it is the correct email and password
  // otherwise, call done with false

  // See if a user with the given email exists
   Promise.using(getDBConn(), function(connection) {
        return connection.query(`SELECT * from users WHERE username = '${email}' limit 1`)
        .then(function(user) {

            if (user.length < 1) { return done(null, false); }
            // compare passwords - is `password` equal to user.password?

            bcrypt.compare(password, user[0].password_hash, function(err, isMatch) {
              if (err) { return done(err); }

              if (!isMatch) { return done(null, false); }

              return done(null, user);
            });

        }).catch(function(error) {
          console.log(error);
          return next(err);
        });
    })
});

// Setup options for JWT Strategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: key.secret
};

// Create JWT strategy
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
  // See if the user ID in the payload exists in our database
  // If it does, call 'done' with that other
  // otherwise, call done without a user object
 Promise.using(getDBConn(), function(connection) {
        const email = req.body.email;
        return connection.query(`SELECT * from users WHERE username = '${payload.sub}' limit 1`)
        .then(function(user) {

          if (user) {
            done(null, user);
          } else {
            done(null, false);
          }

        }).catch(function(error) {
          console.log(error);
          return next(err);
        });
    })
});

// Tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);
